# Digest Exam Project Configuration

1. After cloning, create a database called "senica_digestexam" in your mysql database.
2. Run npm install
3. After a successful package installation, serve the project through this command: `php artisan serve
4. Access the project through the url displayed in the terminal/cmd (e.g. http://localhost:8000)

