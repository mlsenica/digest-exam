<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Digest Exam</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ request()->root() }}{{ mix('css/app.css') }}">
    <style>
        body { font: Montserrat; }
    </style>
</head>
<body>
<div class="container mt-5 mb-5">
    <div id="app">
        <router-view></router-view>
    </div>  
</div>
<script>
    // @README Workaround in retirieving data (since there is API endpoint provided)
    // -- Save data to local storage so Vue SPA can retrieve it
    localStorage.setItem('DIGEST_EXAM_DATA', JSON.stringify({!! $digest !!}));
</script>
<script src="{{ request()->root() }}{{ mix('js/app.js') }}"></script>
        
</body>
</html>
