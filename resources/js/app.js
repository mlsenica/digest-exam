/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';
import router from './core/routes';

const app = new Vue({
    el: '#app',
    router,
    mounted () {
        document.dispatchEvent(new Event('render-event'))
    }
});
